import numpy as np
import cv2 as cv
import os
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import time 
from scipy import ndimage
from skimage import exposure
from PIL import ImageEnhance, Image, ImageOps, ImageFilter
from math import *
import skimage

 ## ------------------ Fonction
def CalculeDesMatricesDePerspective(src, dst):
    M = cv.getPerspectiveTransform(src, dst)
    M_inv = cv.getPerspectiveTransform(dst, src)
    return M, M_inv

def RegionOfInterest(img, vertices):
    mask = np.zeros_like(img)
    if len(img.shape) > 2:
        channel_count = img.shape[2]
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255
    cv.fillPoly(mask, vertices, ignore_mask_color)
    masked_image = cv.bitwise_and(img, mask)
    return masked_image

def AppliqueLesMatrices(img, M):
    image_size = (img.shape[1], img.shape[0])
    VueDuDessus = cv.warpPerspective(img, M, image_size, flags=cv.INTER_LINEAR)
    return VueDuDessus
  
def ObtientLesBlancs(image, color): 
  lower = np.uint8([color, color, color])
  upper = np.uint8([255, 255, 255])
  white_mask = cv.inRange(image, lower, upper)
  return white_mask

def cluster(data, maxgap):
  data.sort()
  groups = [[data[0]]]
  for x in data[1:]:
      if abs(x - groups[-1][-1]) <= maxgap:
          groups[-1].append(x)
      else:
          groups.append([x])
  return groups

def PointsCentralsDesLignesBlanches(hist):
  ClusteredData = cluster(hist, 100)
  PointX = []
  for data in ClusteredData:
    PointX.append(int(np.mean(data)))
  return PointX

def OnDessineLaRoute(Dimensions, M_INV, frame, ploty, left_fitx, right_fitx):

    color_warp = np.zeros(Dimensions.shape).astype(np.uint8)

    pts_left = np.array([np.transpose(np.vstack([left_fitx, ploty]))])
    pts_right = np.array([np.flipud(np.transpose(np.vstack([right_fitx, ploty])))])
    pts = np.hstack((pts_left, pts_right))

    cv.fillPoly(color_warp, np.int32([pts]), (0,255, 0))

    newwarp = cv.warpPerspective(color_warp, M_INV, (Dimensions.shape[1], Dimensions.shape[0])) 

    return cv.addWeighted(frame, 1, newwarp, 0.3, 0)

def FenetreGlissante(image):
  height, width = image.shape
  x = width - 1
  HeightStopNumber = 15
  HeightOfOneStop = ceil(height / HeightStopNumber)

  CenterImage = int(x/2)

  PgX = []
  PgY = []
  PdX = []
  PdY = []

  for HeightStop in range(0, HeightStopNumber):
    PositionY = HeightOfOneStop * HeightStop
    NextPositionY = PositionY + HeightOfOneStop
    Hist = [sum(x) for x in zip(*image[PositionY:NextPositionY, 0:x])]
    PointX = PointsCentralsDesLignesBlanches([i for i, x in enumerate(Hist) if x == max(Hist)])

    for ptn in PointX:
      if(ptn < CenterImage*0.8):
        PgX.append(ptn)
        PgY.append(round((NextPositionY+PositionY)/2))
      if(ptn > CenterImage*1.2):
        PdX.append(ptn)
        PdY.append(round((NextPositionY+PositionY)/2))

  PdX = np.insert(PdX, 0, np.mean(PdX), axis=0)
  PdY = np.insert(PdY, 0, 10, axis=0)

  PgX = np.insert(PgX, 0, np.mean(PgX), axis=0)
  PgY = np.insert(PgY, 0, 10, axis=0)

  PdX = np.insert(PdX, 0, np.mean(PdX), axis=0)
  PdY = np.insert(PdY, 0, 450, axis=0)

  PgX = np.insert(PgX, 0, np.mean(PgX), axis=0)
  PgY = np.insert(PgY, 0, 450, axis=0)

  return PgX, PgY, PdX, PdY
## ------------------ Fin Fonction

NBRIMAGE = 0
ID_VIDEO = 48
cap = cv.VideoCapture("../Prise de vue/output-" + str(ID_VIDEO) + ".avi")

while(True):
    ret, frame = cap.read()

    ## ------------------ Def : On definit le tableau
    fig = plt.figure(figsize = (10, 10))
    columns = 2
    rows = 2

    fig.add_subplot(rows, columns, 1)
    plt.axis('off')
    plt.title("Prise de vue")
    plt.imshow(frame)

    ## ------------------------------------ Alg : Demarre ici



    ## ------------------ Alg : On definit la ROI de l'image
    ImageAvecRegionOfInterest = frame
    DistanceFromBottom = 75
    DistanceFromSide = 0
    HeightROI = 140
    WidthCenterROI = 175

    bottom_px, right_px = (ImageAvecRegionOfInterest.shape[0] - 1, ImageAvecRegionOfInterest.shape[1] - 1)

    pts = np.array([[DistanceFromSide,(bottom_px - DistanceFromBottom)],[(right_px-WidthCenterROI)/2,(bottom_px-DistanceFromBottom-HeightROI)], [(right_px+WidthCenterROI)/2,(bottom_px-DistanceFromBottom-HeightROI)], [right_px-DistanceFromSide, (bottom_px - DistanceFromBottom)]], np.int32)

    ROI = RegionOfInterest(frame, [pts])

    ## ------------------ Alg : On applique les matrices de Perspective
    src_pts = pts.astype(np.float32)
    dst_pts = np.array([[0, bottom_px], [0, 0], [right_px, 0], [right_px, bottom_px]], np.float32)
    M, M_inv = CalculeDesMatricesDePerspective(src_pts, dst_pts)
    BirdView = AppliqueLesMatrices(frame, M)

    ## ------------------ Alg : On transforme les couleurs et on fais un masque
    BirdViewAugmented = Image.fromarray(BirdView)
    BirdViewAugmented = ImageEnhance.Brightness(BirdViewAugmented).enhance(0.02)
    BirdViewAugmented = ImageOps.autocontrast(BirdViewAugmented)
    BirdViewAugmented = ImageOps.equalize(BirdViewAugmented)
    BirdViewAugmented = ImageEnhance.Contrast(BirdViewAugmented).enhance(10)

    kernel = np.ones((5, 5), np.uint8)

    mask = cv.dilate(ObtientLesBlancs(np.array(BirdViewAugmented).astype("uint8"), 150), kernel, iterations=1)

    ## ------------------ Alg : On transforme les couleurs et on fais un masque en utlisant le filtre Minimum
    BirdViewMinimum = ndimage.minimum_filter(BirdViewAugmented, size=10)
    BirdViewMinimum = exposure.adjust_log(ndimage.gaussian_filter(BirdViewMinimum, 10), 0.2)
    BirdViewMinimum = Image.fromarray(BirdViewMinimum)
    BirdViewMinimum = ImageOps.equalize(BirdViewMinimum)
    BirdViewMinimum = np.array(BirdViewMinimum).astype("uint8")
    BirdViewMinimum = ObtientLesBlancs(BirdViewMinimum, 225)

    fig.add_subplot(rows, columns, 2)
    plt.axis('off')
    plt.title("Lignes Blanches")

    BirdViewFinal = cv.add(mask, BirdViewMinimum)

    plt.imshow(BirdViewFinal)
    
    ## ------------------ Alg : On obtient les parties blanches
    fig.add_subplot(rows, columns, 3)
    plt.axis("off")
    plt.title("Regressions polynomiales")

    plt.imshow(BirdView)

    ## ------------------ Alg : On creer les points et on les groupe et on les affiches
    PgX, PgY, PdX, PdY = FenetreGlissante(BirdViewFinal)

    ImageHeight, ImageWidth = BirdViewFinal.shape

    left_fit = np.polyfit(PgY,PgX, 2)
    right_fit = np.polyfit(PdY, PdX, 2)

    lane_width_px=400
    lane_center_px_psp=300

    ploty = np.linspace(0, ImageHeight-1, ImageHeight)
    y_eval = np.max(ploty)

    left_curverad = ((1 + (2 * left_fit[0] * y_eval + left_fit[1])**2)**1.5) / np.absolute(2 * left_fit[0])
    right_curverad = ((1 + (2 *right_fit[0] * y_eval + right_fit[1])**2)**1.5) / np.absolute(2 * right_fit[0])

    center_offset = (((left_fit[0] * y_eval**2 + left_fit[1] * y_eval + left_fit[2]) + (right_fit[0] * y_eval**2 + right_fit[1] * y_eval + right_fit[2])) / 2) - lane_center_px_psp

    print("Left Curvature : " + str(left_curverad))
    print("Right Curvature : " + str(right_curverad))
    print("Center : " + str(center_offset))

    left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
    right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]

    plt.plot(left_fitx, ploty, color='green', linewidth=5)
    plt.plot(right_fitx, ploty, color='pink', linewidth=5)

    fig.add_subplot(rows, columns, 4)
    plt.axis("off")
    plt.title("Sortie Finale")
    colored_lane = OnDessineLaRoute(BirdView, M_inv, frame, ploty, left_fitx, right_fitx)

    plt.imshow(colored_lane)

    directory = "output" + str(ID_VIDEO)
    if not os.path.exists(directory):
      os.makedirs(directory)
    plt.savefig(directory + "/" + str(NBRIMAGE) + ".png")

    NBRIMAGE = NBRIMAGE + 1

cap.release()
